<?php

use App\Http\Controllers\Api\Post\PostController;
use App\Http\Controllers\Api\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class, 'login']);
Route::post('register', [UserController::class, 'register']);
//Route::post('logout', [UserController::class, 'logout']);
Route::group(['middleware' => 'auth:sanctum'], function(){
    Route::post('logout',[UserController::class,'logout']);

    Route::post('new-post', [PostController::class, 'store']);
    Route::post('/update/{id}', [PostController::class, 'update']);
    Route::delete('/delete/{post}', [PostController::class, 'destroy']);

    Route::get('my-posts', [PostController::class, 'myPosts']);

});

Route::get('posts', [PostController::class, 'index']);
Route::get('post/{id}', [PostController::class, 'post']);

