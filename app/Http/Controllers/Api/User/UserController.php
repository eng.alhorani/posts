<?php

namespace App\Http\Controllers\Api\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\API\LoginUserRequest;
use App\Http\Requests\API\UserRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
class UserController extends Controller
{

    function login(Request $request)
    {
        $user= User::where('email', $request->email)->first();
        // print_r($data);
        if (!$user || !Hash::check($request->password, $user->password)) {
            return response([
                'message' => ['These credentials do not match our records.']
            ], 404);
        }

        $token = $user->createToken('my-app-token')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }
    public function register(UserRequest $request){
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->save();
        $token=$user->createToken('my-app-token')->plainTextToken;
        $respons=[
            'user'=>$user,
            'token'=>$token
        ];
        return $respons;
    }

    public function logout(Request $request){
        auth()->user()->tokens()->delete();
    }

}
