const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

$('document').ready(function () {
   
    
   
    $('#example').DataTable( {
        
        "ajax": "/table",
        "columns": [
            { "data": "numberNotes" },
            { "data": "type" }
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ]
    } );



    fetchChartData();


   
});

function fetchChartData() {
  
    $.ajax({
        method: 'GET',
        url: '/type'
    }).done(function (data) {
        console.log(data);
        Type(data);
    });


   
   
}

function Type(data) {

    var element = document.getElementById("chart_2");

    if (!element) {
        return;
    }
    var options = {

        series: data.noteNumber,
        // labels: ['normal','on date','urgent'],
        labels: data.type,

        // series: [data.ordering , data.notOrdering],
        chart: {
            type: 'donut',
        },
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                }
            }
        }],
        colors: [success, warning , info ]
    };

    var chart = new ApexCharts(element, options);
    chart.render()
}

   