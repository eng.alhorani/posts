<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>create post</title>
    @include('includes._header')
</head>
<body class="antialiased">
@include('includes._nav')

<div class="container">


    <div class="card card-default">
        <div class="card-header">
         {{isset($post) ? "update post":"add new post"}}
        </div>
        <div class="card-body">
            <form action="{{isset($post) ? route('update',$post->id) : route('store') }}" method="POST" id="formId" enctype="multipart/form-data">
                @csrf
                @if (isset($post))
                    @method('PUT')
                @endif
                <div class="form-group">
                    <label for="post title">Title:</label>
                    <input type="text" class="form-control" name="title" placeholder="Add a new post" value="{{ isset($post) ? $post->title : "" }}">
                </div>
                <div class="form-group">
                    <label for="post content">Content:</label>
                    <textarea class="form-control @error('content') is-invalid @enderror " rows="2" name="content" placeholder="Add a content">{{ isset($post) ? $post->content : "" }}</textarea>
                    @error('content')
                    <div class="invalid-feedback " role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
{{--        <div>--}}
{{--            <label for="post image">Type:</label>--}}
{{--            <select name="type" class="custom-select custom-select-lg mb-3">--}}
{{--                <option selected>Open this select menu</option>--}}
{{--                <option value="urgent">urgent</option>--}}
{{--                <option value="normal">normal</option>--}}
{{--                <option value="on date">on date</option>--}}
{{--            </select>--}}
{{--        </div>--}}


                <div class="form-group">
                    <label for="post image">Image:</label>
                    <input type="file" class="form-control @error('cover_image') is-invalid @enderror" name="cover_image">
                    @error('cover_image')
                    <div class="invalid-feedback " role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                      {{ isset($post) ? "Update" : "Add" }}
                    </button>
                </div>
            </form>
        </div>
    </div>



</div>
@include('includes._scripts')
<script>

</script>

</body>
</html>
